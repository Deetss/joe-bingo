import React, { Component } from 'react';
import './App.css';
import { Board, BoardTitle } from './Board/Board';
import { SignupModal } from "./Modals/SignupModal";
import firebase from './firebase.js';


class App extends React.Component {
  constructor(props){
    super(props);
    this.state = { user: null };
  }

  handleLogin() {
    var user = firebase.auth().currentUser;

    console.log('HANDLE LOGIN FUNC', user);
    if (user) {
      // User is signed in.
      // var displayName = user.displayName;
      // var email = user.email;
      // var emailVerified = user.emailVerified;
      // var photoURL = user.photoURL;
      // var isAnonymous = user.isAnonymous;
      // var uid = user.uid;
      // var providerData = user.providerData;
      this.setState(() => {
        return {user};
      });
      App.forceUpdate();
    } else {
      this.setState(() => {
        return {user: null};
      });
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        {this.state.user ? this.state.user.email : <SignupModal handleLogin={this.handleLogin.bind(this)}/>}
          <h1 className="App-title">Welcome to Joe Bingo</h1>
        </header>
        <BoardTitle />
        <Board />
      </div>
    );
  }
}

export default App;
