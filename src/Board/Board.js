import React from "react";


const boardItems = [
  // row 1
  'Joe mentions "Ecommerce"',
  'Joe applies for a job',
  'Joe mentions VB for Android',
  'Joe says "K"',
  'Joe "Cant figure out rails"',
  // row 2
  'Joe posts something random',
  'Joe mentions how busy he is',
  'Joe jumps into a conversation with "what?"',
  'Joe posts a link to digg	',
  'Joe posts a random youtube video',
  // row 3
  'Joe responds "Idk" to someones questions',
  'Joe talks about hockey',
  ':wave:',
  'Joe impressed someone',
  'Joe gets a point',
  // row 4
  'Joe uses a buzzword innapropriately',
  'Joe says "brb getting lunch"',
  'Joe says "gtg bye" to himself',
  'Joe whines about the accounting lady',
  'Joe promises he will start learning ruby today',
  // row 5
  'One of Joes "Servers" go down',
  'Joe answers himself after 30+ minutes',
  'Joe sees a popular movie and declares it boring',
  'Joe says "what\'s new"',
  'Joe says a language "is not hard'
];

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const Wave = () => {
  return (
    <img src="https://cdn01.gitter.im/_s/fd52102/images/emoji/wave.png" alt="Wave"></img>
  )
}


class Cell extends React.Component {
  constructor(props) {
    super(props);
    shuffle(boardItems);
    this.state = {isToggled: false}

    this.handleClick = this.handleClick.bind(this);
  };

  handleClick() {
    this.setState(prevState => ({
      isToggled: !prevState.isToggled
    }));
    console.log('its firing');
  }

  render(){
    return (
      <div className={this.state.isToggled ? 'col marked' : 'col'}  onClick={this.handleClick}>
      {this.props.quote}
      </div>
    );
  }
}

const BoardTitle = () => {
  return (
  <div className="board__title">
    <span>M</span>
    <span>E</span>
    <span>R</span>
    <span>L</span>
    <span>I</span>
    <span>N</span>
  </div>
  )
}

class Board extends React.Component {

  constructor(props) {
    super(props);
    shuffle(boardItems);
  };

  render(){
    return (
      <div className="board__container">
        {boardItems.map(item => {
          if (item === ':wave:') {
          return <Cell quote={<Wave/>}/>
            } else {
              return <Cell quote={item}/>
            }
          })
        }
      </div>
      );
  }

}

export { Board, BoardTitle};