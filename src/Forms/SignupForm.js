import React from 'react';
import firebase from '../firebase.js';

export class SignupForm extends React.Component {
  constructor(props) {
    super(props);
  }
  handleSignUp(e) {
    e.preventDefault();

    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    if (email.length < 4) {
      alert('Please enter an email address.');
      return;
    }
    if (password.length < 4) {
      alert('Please enter a password.');
      return;
    }
    // Sign in with email and pass.
    // [START createwithemail]
    firebase.auth().createUserWithEmailAndPassword(email, password).then(user => {
      firebase.auth().signInWithEmailAndPassword(email, password).then(data => {
        console.log(data);
        this.props.handleLogin();
        this.props.closeModal();
      }).catch(function(error) {
        // handle errors
      });
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // [START_EXCLUDE]
      if (errorCode == 'auth/weak-password') {
        alert('The password is too weak.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
      // [END_EXCLUDE]
    });
    // [END createwithemail]
  }

  render() {
      return(
          <form onSubmit={(e) => this.handleSignUp(e)}>
              <label htmlFor="email" /> Email
              <input type="text" name="email" id="email" />
              <label htmlFor="password" /> Password
              <input type="password" name="password" id="password" />

              <button type="submit">Sign Up!</button>
          </form>
      );
  }
}
