// Import the Firebase modules that you need in your app.
import firebase from 'firebase';
import 'firebase/auth';

console.log(firebase)

// Initialize Firebase
  var config = {
    apiKey: "AIzaSyChGJ7xBbp0Fn-qJ4Zx4Pi1k5IRZXlDBwc",
    authDomain: "joe-bingo.firebaseapp.com",
    databaseURL: "https://joe-bingo.firebaseio.com",
    projectId: "joe-bingo",
    storageBucket: "joe-bingo.appspot.com",
    messagingSenderId: "33485969632"
  };

export default firebase.initializeApp(config);